<?php

function rules_login_rules_action_info() {
  return array(
    'rules_login_login' => array(
      'label' => t('Log in'),
      'group' => t('User'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to login'),
        ),
      ),
    ),
    'rules_login_logout' => array(
      'label' => t('Log out'),
      'group' => t('User'),
    ),
  );
}
